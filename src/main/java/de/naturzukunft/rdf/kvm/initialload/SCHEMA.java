package de.naturzukunft.rdf.kvm.initialload;


import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public class SCHEMA {

	public static final String NAMESPACE = "https://schema.org/";
	public static final String PREFIX = "schema";
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);
	public static final ValueFactory factory = SimpleValueFactory.getInstance();
	
	// ----- Classes ------
	public final static IRI CONTACT_POINT = factory.createIRI(NAMESPACE, "ContactPoint");

	// ----- Properties ------
	public final static IRI IDENTITIER = factory.createIRI(NAMESPACE, "identifier");
	public final static IRI FOUNDED_DATE = factory.createIRI(NAMESPACE, "foundingDate");
	public final static IRI LATITUDE = factory.createIRI(NAMESPACE, "latitude");
	public final static IRI LONGITUDE = factory.createIRI(NAMESPACE, "longitude");
	public final static IRI NAME = factory.createIRI(NAMESPACE, "name");
	public final static IRI EMAIL = factory.createIRI(NAMESPACE, "email");
	public final static IRI TELEPHONE = factory.createIRI(NAMESPACE, "telephone");
	
	
}
